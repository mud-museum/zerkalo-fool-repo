/* ************************************************************************
*   File: prool.h                            Part of Zerkalo MUD          *
*  Usage: prool subprograms for Zerkalo MUD                               *
*                                                                         *
*  (CC) 2012-2024 Prool                                                   *
*                                                                         *
*  Author: Prool, proolix@gmail.com, http://prool.kharkov.org,            *
*  http://virtustan.net http://virtustan.tk                               *
************************************************************************ */

#define PROOL_G_LEN 1024
#define PROOL_MAX_STRLEN 1024
#define PROOL_DESCR_LEN 70
#define BUFLEN 512

#define DUKHMADA_FILE "dukhmada.cfg"
#define NABORY_FILE "nabory.cfg"
#define PERSLOG_FILE "../log/perslog.txt"

extern int prool_flag_quiet;
extern char prool_g_buf[];

char *ptime(void);
char	*to_utf(char *str);
void make_who2html(void);
//int can_take_obj(CharacterData * ch, OBJ_DATA * obj);
void perslog (char *verb, const char *pers);
void send_email2 (char *from, char *to, char *subj, char *text);
void system_(char *cmd);
void send_telegram(char *msg);

// prool commands:
void do_fish(CharacterData *ch, char *argument, int cmd, int subcmd);
void do_fflush(CharacterData *ch, char *argument, int cmd, int subcmd);
void do_duhmada_e(CharacterData *ch, char *argument, int cmd, int subcmd);
void do_get_nabor(CharacterData *ch, char *argument, int cmd, int subcmd);
void do_accio_trup(CharacterData *ch, char *argument, int cmd, int subcmd);
void do_shutdown_info (CharacterData *ch, char * /*argument*/, int/* cmd*/, int/* subcmd*/);
void do_kogda (CharacterData *ch, char *argument, int/* cmd*/, int/* subcmd*/);
void do_proolflag (CharacterData *ch, char *arg, int/* cmd*/, int/* subcmd*/);
void do_descr (CharacterData *ch, char *arg, int/* cmd*/, int/* subcmd*/);
void do_catfile (CharacterData *ch, char *arg, int/* cmd*/, int/* subcmd*/);
void do_megarecall(CharacterData *ch, char* /*argument*/, int/* cmd*/, int/* subcmd*/);
void do_prooltest (CharacterData *ch, char *arg, int/* cmd*/, int/* subcmd*/);
void do_prool_skillset(CharacterData *ch, char * /*argument*/, int/* cmd*/, int/* subcmd*/);
void do_megacheater(CharacterData *ch, char * /*argument*/, int/* cmd*/, int/* subcmd*/);
// end of prool commands
