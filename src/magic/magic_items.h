#ifndef __MAGIC_ITEMS_H__
#define __MAGIC_ITEMS_H__

class CharacterData;
class ObjectData;

void employMagicItem(CharacterData *ch, ObjectData *obj, const char *argument);

#endif //BYLINS_CREATE_H
