Zerkalo readme from Prool

This is Zerkalo - multiusers dungeon (MUD) engine (MUD server).
Zerkalo is derivative of Byliny (Bylins) MUD.
Byliny is derivative of Circle MUD.

Language of Zerkalo and Byliny is Russian.

---

This branch ("prool-4001") is current production version in my server mud.kharkov.org:4001
(shard for builders)

Branch "prool-4000" is working on port 4000.
 
---

Зеркало. Текст от Пруля.

Зеркало это форк Былин, а Былины это потомок кодовой базы CircleMUD.

Отличия Зеркала от оригинальных Былин весьма небольшие, в первую очередь направленные
на оказуаливание, то есть упрощение игры, потому как в оригинальным Былинах игра
довольно хардкорна (трудна), хотя и там в последнее время убирают самые суровые
элементы игры, например плату за постой.

---

Как самому собрать Зеркало (или Былины)

Смотри также оригинальный readme-byliny.md в каталоге old. Без проблем собирается в Debian/Ubuntu 64 bit или в Windows 64 bit
в средах cygwin или WSL.

Кратко:

Распаковать

Войти в каталог мада (тот, в котором данный файл и файл CMakeLists.txt)

mkdir build

cp cmake1.sh build

cd build

./cmake1.sh

make

На данном этапе может оказаться, что не хватает каких-то библиотек. Тогда читайте оригинальный readme.

---

Также я смог собрать этот код в Windows 7 32 bit и macOS Catalina.

---

Как запустить

Файл circle (circle.exe для Windows), переместить из каталога build, где
он создастся при успешной компиляции, в каталог bin.

Затем надо набрать bin/circle (находясь в том каталоге, в котором находится данный readme файл и подкалог bin).
И мад запустится на порту 4000. Запустится мад с заставкой оригинальных Былин, двоичным кодом Зеркала
(который, как я уже говорил, суть слегка модифицированные Былины) и тремя начальными зонами.
Полный мир Зеркала можно взять отсюда https://gitlab.com/prool/new-zerkalo-mud-world-utf

Примечание: для сборки в среде 32-разрядного cygwin (то есть в 32-разрядных Виндах) надо перед тем, как делать
make, раскомментировать строку

//#define CYGWIN32

расположенную в начале файла db.cpp

Двоичный файл, полученный внутри cygwin, будет запускаться только из среды cygwin. Чтобы запускать его из Виндовс, придется
найти в положить в каталог bin нужные для работы dll файлы (в количестве около пяти). Какие файлы нужны, скажет сам бинарник,
при запуске, когда выдаст сообщени об ошибке типа "Не могу запуститься, ищу файл cygwin.dll). Все эти файлы берутся из
каталога c:/cygwin/bin

---

Пруль, 2024 год, Харьков
http://prool.kharkov.org
http://mud.kharkov.org
http://zerkalo.kharkov.org
http://virtustan.tk
http://virtustan.net

