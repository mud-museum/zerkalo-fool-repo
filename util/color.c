// 9 and 256 color generator.
// prool

#include <stdio.h>

void main (void)
{
for (int i=30;i<=39;i++) {
	printf("\x1b[%imCOLOR%i ", i, i);
	}
printf("\r\n");

for (int i=30;i<=39;i++) {
	printf("\x1b[1;%imBOLD %i ", i, i);
	}
printf("\r\n");

for (int i=0; i<256; i++) {
	printf("\x1b[38;5;%im%03i ", i, i);
	}
printf("\r\n");
}
