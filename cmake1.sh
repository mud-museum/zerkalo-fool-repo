#!/bin/sh
#
# Instruction for build MUD in Linux
#
# Enter to MUD root dir (where CMakeLists.txt)
# mkdir build
# cp cmake1.sh build
# cd build
# ./cmake1.sh
# make
# cp circle ../bin
# cd ..
# bin/circle
#
# PROFIT!
#
# Prool, proolix@gmail.com, http://prool.kharkov.org http://virtustan.net
# 23 Feb 2024, Kharkiv, Ukraine
#
cmake -DSCRIPTING=NO -DBUILD_TESTS=NO -DCMAKE_BUILD_TYPE=Release ..
